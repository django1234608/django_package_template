## django-package


package is a Django app to manage ... 
Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add the application to your INSTALLED_APPS setting like this::

    INSTALLED\_APPS = [
        ...,
        "package.apps.PackageConfig",
    ]

2. Include the patients URLconf in your project urls.py like this:

    path("package/", include("package.urls")),

3. Run ``python manage.py migrate`` to create the models.



